var SerialPort = require('serialport');
let dataaArr = [];

function round(value, decimals) {
  return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
}

function averageValues(arr) {
  let keys = Object.keys(arr[0]);
  let obj = {};
  for (let i = 0; i < arr.length; i++) {
    keys.map(k => {
      if (!obj[k])
        obj[k] = 0;
      obj[k] += arr[i][k];
    })
  }
  keys.map(k => {
    obj[k] = round(obj[k] / arr.length, 1)
  })
  return obj;
}

// list serial ports:
SerialPort.list(function (err, ports) {
  ports.forEach(function (port) {
    name = port.comName;
  });
}).then(function (ports) {
  console.log('serial ports data:', ports)

  var myPort;
  // var myPort = new SerialPort('COM7', 9600);
  // var myPort = new SerialPort(ports[0].comName, 9600);
  for (let i = 0; i < ports.length; i++) {
    if (ports[i].locationId) {
      myPort = new SerialPort(ports[i].comName, 9600);
      console.log(ports[i].comName)
      break;
    }
  }

  var Readline = SerialPort.parsers.Readline; // make instance of Readline parser
  var parser = new Readline(); // make a new parser to read ASCII lines
  myPort.pipe(parser); // pipe the serial stream to the parser

  parser.on('data', readSerialData);

  var firebase = require('firebase').initializeApp({
    // serviceAccount: './sepi-node-firebase-86991f916ae1.json',
    // databaseURL: 'https://sepi-node-firebase.firebaseio.com/'
    databaseURL: 'https://sepi-node-firebase.firebaseio.com/'
    // databaseURL: 'https://heikkihosting.firebaseio.com/'
  });


  function readSerialData(data) {
    // check that last add was over 5min ago
    // if (lastAddTime && new Date() - lastAddTime < 60000*.5) {
    //   return;
    // }

    if (data.length < 10)
      return;
    let arr = data.match(/[a-z]{2}|-?[0-9]{1,2}.[0-9]/g);
    let obj = {};
    for (let i = 0; i < arr.length; i++) {
      obj[arr[i]] = Number.parseFloat(arr[++i]);
    }


    dataaArr.push(obj);

    console.log('raw:', data);
    if (dataaArr.length < 30) // kun taulukossa on 30 kpl 
      return;

    var parsedDataObject = averageValues(dataaArr);
    console.log('=========================')
    console.log(new Date().toString())
    console.log('parsed:', parsedDataObject);
    console.log('=========================')

    dataaArr = [];

    // console.log(JSON.stringify(parsedDataObject,undefined, 2));
    // console.log();

    var message = {
      data: parsedDataObject,
      aika: Date.parse(new Date()),
    };

    var ref = firebase.database().ref();
    var messagesRef = ref.child('lampo');
    messagesRef.push(message);

    lastAddTime = new Date();
  }
})

/*
  ov: menoveden ohjelämpötila
  mv: menoveden lämpötila
  pv: paluveden lämpötila
  ul: ulkolämpötila
  sl: sisälämpötila
  kl: kattilalämpötila
  kv: käyttöveden lämpötila
  oh: ohjaus
*/